import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Character } from 'src/app/models/character';
import { NgxHmCarouselBreakPointUp } from 'ngx-hm-carousel';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-carousel-characters',
  templateUrl: './carousel-characters.component.html',
  styleUrls: ['./carousel-characters.component.scss']
})
export class CarouselCharactersComponent implements OnInit {

  @Input()
  characters: Array<Character> = [];

  @Output()
  indexCharacter = new EventEmitter();

  indexEvent = 0;

  showNum = 'auto';
  currentIndex = 0;

  breakpoint: NgxHmCarouselBreakPointUp[] = [
    {
      width: 475,
      number: 1
    },
    {
      width: 800,
      number: 3
    },
    {
      width: 1024,
      number: 5
    },
  ];

  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.iconRegistry.addSvgIcon(
      'left-arrow',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/icon/left_icon.svg')
    );
    this.iconRegistry.addSvgIcon(
      'right-arrow',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/icon/right_icon.svg')
    );
  }

  updateCurrentIndex(newIndex) {
      this.indexEvent = newIndex;
      this.indexCharacter.emit(this.indexEvent);
  }
}
