import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselCharactersComponent } from './carousel-characters.component';

describe('CarouselCharactersComponent', () => {
  let component: CarouselCharactersComponent;
  let fixture: ComponentFixture<CarouselCharactersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselCharactersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselCharactersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
