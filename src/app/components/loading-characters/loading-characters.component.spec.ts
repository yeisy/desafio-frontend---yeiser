import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingCharactersComponent } from './loading-characters.component';

describe('LoadingCharactersComponent', () => {
  let component: LoadingCharactersComponent;
  let fixture: ComponentFixture<LoadingCharactersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingCharactersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingCharactersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
