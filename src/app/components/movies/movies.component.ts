import { Component, OnInit } from '@angular/core';
import { Film } from '../../models/film';
import { FilmsService } from '../../services/films.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CharactersService } from 'src/app/services/characters.service';
import { Character } from 'src/app/models/character';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  film: Film;
  characters: Array<Character> = [];
  loadingCharacters = true;
  loadedChars = 0;

  constructor(
    private filmService: FilmsService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private characterService: CharactersService,
    private router: Router
  ) { }

  ngOnInit() {


    this.route.params.subscribe(params => {

      this.film = this.filmService.getFilmFromLocalStorageByURL('https://swapi.co/api/films/' + params['id']);

      if (this.film === undefined) {
        this.filmService.getSingleFilmById(params['id']).subscribe(data => {
          this.film = data;
          this.loadCharacters();
        },
        error => {
            this.router.navigate(['/']);
        });
      }
    });

    this.iconRegistry.addSvgIcon(
      'users_icon',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/users.svg")
    );

    this.iconRegistry.addSvgIcon(
      'user_icon_gray',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/user-circle-gray.svg")
    );

    this.iconRegistry.addSvgIcon(
      'comments_icon',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/comments.svg")
    );

    this.iconRegistry.addSvgIcon(
      'thumbs_down',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/thumbs-down.svg")
    );
    this.iconRegistry.addSvgIcon(
      'thumbs_up',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/thumbs-up.svg")
    );
  }

  loadCharacters() {
    let character;

    for (let i = 0; i < this.film.characters.length; i++) {
      let charUri = this.film.characters[i];

      character = this.characterService.getCharacterFromLocalStorageByURL(charUri);

      if (character === undefined) {
        this.characterService.getSingleCharacter(charUri).subscribe(data => {
          this.characters.push(data);
          this.loadedChars++;

          if (this.loadedChars === this.film.characters.length) {
            this.loadingCharacters = false;
          }
        });
      } else {
        this.characters.push(character);
        this.loadedChars++;
      }
    }
  }

}
