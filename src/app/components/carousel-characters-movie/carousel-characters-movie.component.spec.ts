import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselCharactersMovieComponent } from './carousel-characters-movie.component';

describe('CarouselCharactersMovieComponent', () => {
  let component: CarouselCharactersMovieComponent;
  let fixture: ComponentFixture<CarouselCharactersMovieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselCharactersMovieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselCharactersMovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
