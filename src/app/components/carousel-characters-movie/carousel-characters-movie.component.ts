import { Component, OnInit, Input } from '@angular/core';
import { Character } from 'src/app/models/character';
import { ResponseApi } from 'src/app/models/responseApi';
import { NgxHmCarouselBreakPointUp } from 'ngx-hm-carousel';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-carousel-characters-movie',
  templateUrl: './carousel-characters-movie.component.html',
  styleUrls: ['./carousel-characters-movie.component.scss']
})
export class CarouselCharactersMovieComponent implements OnInit {

  @Input()
  characters: Array<Character>;

  responseApi: ResponseApi;

  loadedCharacters = 0;

  currentIndex = 0;
  breakpoint: NgxHmCarouselBreakPointUp[] = [
    {
      width: 375,
      number: 1
    },
    {
      width: 500,
      number: 1
    },
    {
      width: 1024,
      number: 3
    },
    {
      width: 1200,
      number: 5
    }
  ];

  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) { }


  ngOnInit() {
    this.iconRegistry.addSvgIcon(
      'left-arrow',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/left_icon.svg")
    );
    this.iconRegistry.addSvgIcon(
      'right-arrow',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/right_icon.svg")
    );
  }


}
