import { Component, OnInit } from '@angular/core';
import { Reviews } from 'src/app/models/reviews';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ReviewService } from 'src/app/services/review.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {

  reviewsForm: FormGroup;
  reviews: Array<Reviews> = [];
  review: Reviews;

  constructor(private fb: FormBuilder, private reviewService: ReviewService) {
    this.createForm();
  }

  ngOnInit() {
    this.reviews = this.reviewService.getObjectsFromLocalStorage('reviews') as Array<Reviews>;

    if(this.reviews === undefined){
      this.reviews = [];
    }

  }

  createForm() {
    this.reviewsForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      comment: ['', Validators.required]
    });
  }

  onSubmit() {
    let review = {
      name: this.reviewsForm.value.name,
      email: this.reviewsForm.value.email,
      comment: this.reviewsForm.value.comment,
      likes: 0,
      dislikes: 0,
    };

    this.reviews.unshift(review);
    this.reviewService.saveObjectToLocalStorage(review, 'reviews', false);
    this.reviewsForm.reset();
  }

  addLikes(index: number, like: boolean) {
    if (like) {
      this.reviewService.updateReviewLikes(index, true);
      this.reviews[index].likes += 1;
    } else {
      this.reviewService.updateReviewLikes(index, false);
      this.reviews[index].dislikes += 1;
    }

  }
}
