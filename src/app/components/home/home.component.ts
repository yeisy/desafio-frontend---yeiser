import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { Character } from 'src/app/models/character';
import { Film } from 'src/app/models/film';

import { FormBuilder, FormGroup } from '@angular/forms';
import { FilmsService } from 'src/app/services/films.service';
import { CharactersService } from 'src/app/services/characters.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private _searchTerm: string;

  films: Array<Film> = [];
  filteredFilms: Array<Film> = [];

  characters: Array<Character> = [];
  filteredCharacters: Array<Character> = [];

  loadingFilms: boolean;
  loadingCharacters: boolean;

  options: FormGroup;
  showResults: boolean = false;
  currentIndex = 0;
  totalCharacters = 0;
  page = 1;

  constructor(
    private filmService: FilmsService,
    private charactersService: CharactersService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer, private fb: FormBuilder) { }


  ngOnInit() {

    this.loadingFilms = true;
    this.loadingCharacters = true;


    this.films = this.filmService.getObjectsFromLocalStorage('films') as Array<Film>;

    if (this.films === undefined) {
      this.filmService.getFilms().subscribe(data => {
        this.films = data.results;
        this.filteredFilms = this.films;
        this.loadingFilms = false;
        this.filmService.replaceObjectInLocalStorage(this.films, 'films');
      });
    }
    else{
      this.filteredFilms = this.films;
      this.loadingFilms = false;
    }

    this.characters = this.charactersService.getObjectsFromLocalStorage('characters') as Array<Character>;

    if (this.characters === undefined) {
      this.charactersService.getCharactersPerPage(this.page).subscribe(data => {
        this.characters = data.results;
        this.filteredCharacters = this.characters;
        this.totalCharacters = data.count;
        this.loadingCharacters = false;
        this.charactersService.replaceObjectInLocalStorage(this.characters, 'characters');
        this.charactersService.setValue( this.totalCharacters, 'total-chars');
      });
    }
    else{
      this.filteredCharacters = this.characters;
        this.totalCharacters = parseInt(this.charactersService.getValue('total-chars'));
        this.loadingCharacters = false;

    }


    this.options = this.fb.group({
      color: 'accent',
    });

    this.iconRegistry.addSvgIcon(
      'star_icon',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/icon/star.svg')
    );

    this.iconRegistry.addSvgIcon(
      'users_icon',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/icon/users.svg')
    );
  }

  get searchTerm() {
    return this._searchTerm;
  }

  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filteredCharacters = this.filterCharacters(value);
    this.filteredFilms = this.filterFilms(value);

    if (value !== '') {
      this.showResults = true;
    } else {
      this.showResults = false;
    }
  }

  filterCharacters(searchString: string) {
    return this.characters.filter(character => character.name.toLocaleLowerCase().indexOf(searchString.toLocaleLowerCase()) !== -1);
  }

  filterFilms(searchString: string) {
    return this.films.filter(film => film.title.toLocaleLowerCase().indexOf(searchString.toLocaleLowerCase()) !== -1);
  }

  clearSearch() {
    this._searchTerm = '';
    this.showResults = false;
    this.filteredFilms = this.films;
    this.filteredCharacters = this.characters;
  }

  loadMoreCharacters(index: number) {
    let loadedCharacters = this.characters.length;

    if (loadedCharacters < this.totalCharacters) {
      if (index === this.characters.length - 2) {
        {

          this.page = Math.floor( index / 10 ) + 2;
          this.charactersService.getCharactersPerPage(this.page).subscribe(data => {
            this.characters = this.characters.concat(data.results);
            this.filteredCharacters = this.characters;
            this.charactersService.replaceObjectInLocalStorage(this.characters, 'characters');
          });
        }
      }
    }
  }
}
