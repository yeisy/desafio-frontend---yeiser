import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { CharactersService } from '../../services/characters.service';
import { PlanetService } from '../../services/planet.service';
import { Planet } from 'src/app/models/planet';
import { Character } from 'src/app/models/character';

@Component({
  selector: 'app-card-character',
  templateUrl: './card-character.component.html',
  styleUrls: ['./card-character.component.scss']
})


export class CardCharacterComponent implements OnInit {

  @Input() character: Character;

  planet: Planet = <Planet>{};

  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer, private charactersService: CharactersService, private planetService: PlanetService) { }

  ngOnInit() {

    this.planet = this.planetService.getPlanetFromLocalStorageByURL(this.character.homeworld);

    if (this.planet === undefined) {
      this.planetService.getPlanetByUrl(this.character.homeworld).subscribe(data => {
        this.planet = data;

        if (this.planetService.getPlanetFromLocalStorageByURL(this.character.homeworld) === undefined) {
          this.planetService.saveObjectToLocalStorage(this.planet, 'planets');
        }
      });
    }

    this.iconRegistry.addSvgIcon(
      'map_marker_alt',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/map_marker_alt.svg")
    );
  }

}
