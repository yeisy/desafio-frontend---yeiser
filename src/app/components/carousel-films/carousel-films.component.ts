import { Component, OnInit, Input } from '@angular/core';
import { Film } from 'src/app/models/film';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { NgxHmCarouselBreakPointUp } from 'ngx-hm-carousel';


@Component({
  selector: 'app-carousel-films',
  templateUrl: './carousel-films.component.html',
  styleUrls: ['./carousel-films.component.scss']
})
export class CarouselFilmsComponent implements OnInit {


  @Input()
  films: Array<Film> = [];

  currentIndex = 0;
  breakpoint: NgxHmCarouselBreakPointUp[] = [
    {
      width: 550,
      number: 1
    },
    {
      width: 600,
      number: 1
    },
    {
      width: 800,
      number: 3
    }
  ];


  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) { }

  ngOnInit() {


    this.iconRegistry.addSvgIcon(
      'left-arrow',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/icon/left_icon.svg')
    );
    this.iconRegistry.addSvgIcon(
      'right-arrow',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/icon/right_icon.svg')
    );
  }
}
