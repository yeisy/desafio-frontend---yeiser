import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselFilmsComponent } from './carousel-films.component';

describe('CarouselFilmsComponent', () => {
  let component: CarouselFilmsComponent;
  let fixture: ComponentFixture<CarouselFilmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselFilmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselFilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
