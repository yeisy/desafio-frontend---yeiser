import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { Film } from 'src/app/models/film';
import { Router } from '@angular/router';



@Component({
  selector: 'app-card-film',
  templateUrl: './card-film.component.html',
  styleUrls: ['./card-film.component.scss']
})
export class CardFilmComponent implements OnInit {

  @Input() film: Film;

  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit() {
    this.iconRegistry.addSvgIcon(
      'calendar_icon',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/calendar_icon.svg")
    );
    this.iconRegistry.addSvgIcon(
      'chair_director_icon',
      this.sanitizer.bypassSecurityTrustResourceUrl("./assets/icon/chair_director_icon.svg")
    );
  }

  redirectToMovie(url: string) {
    if (url !== undefined) {
      let id = url.split("/");
      this.router.navigate(['movie/', id.reverse()[1]]);
    }
  }

}
