import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingFilmsComponent } from './loading-films.component';

describe('LoadingFilmsComponent', () => {
  let component: LoadingFilmsComponent;
  let fixture: ComponentFixture<LoadingFilmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingFilmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingFilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
