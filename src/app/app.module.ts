import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';

import { AppRoutingModule, routingComponent } from './app-routing.module';

import { AppComponent } from './app.component';
import { ReviewsComponent } from './components/reviews/reviews.component';
import { CarouselFilmsComponent } from './components/carousel-films/carousel-films.component';
import { CarouselCharactersComponent } from './components/carousel-characters/carousel-characters.component';
import { CardFilmComponent } from './components/card-film/card-film.component';
import { CardCharacterComponent } from './components/card-character/card-character.component';

import {FilmsService} from './services/films.service';
import {CharactersService} from './services/characters.service';
import {PlanetService} from './services/planet.service';
import { ReviewService } from './services/review.service';


import {MaterialModule} from './material';
import { CarouselCharactersMovieComponent } from './components/carousel-characters-movie/carousel-characters-movie.component';
import { LoadingFilmsComponent } from './components/loading-films/loading-films.component';
import { LoadingCharactersComponent } from './components/loading-characters/loading-characters.component';


@NgModule({
  declarations: [
    AppComponent,
    ReviewsComponent,
    CarouselFilmsComponent,
    CarouselCharactersComponent,
    CardFilmComponent,
    CardCharacterComponent,
    routingComponent,
    CarouselCharactersMovieComponent,
    LoadingFilmsComponent,
    LoadingCharactersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule, 
    ReactiveFormsModule,
    NgxHmCarouselModule
  ],
  providers: [FilmsService, CharactersService, PlanetService, ReviewService],
  bootstrap: [AppComponent]
})
export class AppModule { }
