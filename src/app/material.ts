import { MatButtonModule, MatCardModule, MatFormFieldModule, MatIconModule,
    MatInputModule,MatCheckboxModule, MatSlideToggleModule } from '@angular/material';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        MatButtonModule, 
        MatCardModule, 
        MatFormFieldModule, 
        MatIconModule,
        MatInputModule,
        MatCheckboxModule,
        MatSlideToggleModule
    ],
    exports: [
        MatButtonModule, 
        MatCardModule, 
        MatFormFieldModule, 
        MatIconModule,
        MatInputModule,
        MatCheckboxModule,
        MatSlideToggleModule
    ]
})
export class MaterialModule { }