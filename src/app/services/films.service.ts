import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResponseApi } from '../models/responseApi';
import { Observable } from 'rxjs';
import { Film } from '../models/film';
import { LocalStorageService } from '../models/LocalStorage';

@Injectable({
  providedIn: 'root'
})
export class FilmsService extends LocalStorageService {

  constructor(private http: HttpClient) {
    super();
  }

  getFilms(): Observable<ResponseApi> {
    return this.http.get<ResponseApi>("https://swapi.co/api/films/");
  }

  getSingleFilmById(id: number): Observable<any> {
   let url = "https://swapi.co/api/films/"+id+"/";
    return this.http.get<Film>(url);
  }


  getFilmFromLocalStorageByURL(uri: string): Film  {
    let films: Film[] = [];
    let film: Film;



    if (localStorage.getItem('films') !== null) {

      films = JSON.parse(localStorage.getItem('films'));

      for (let i = 0; i < films.length; i++) {

        if (films[i].url.localeCompare(uri) === 0) {
          film = films[i];

        }
      }
    }
    return film;
  }

}
