import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Planet } from '../models/planet';
import { LocalStorageService } from '../models/LocalStorage';

@Injectable({
  providedIn: 'root'
})
export class PlanetService extends LocalStorageService {

  constructor(private http: HttpClient) {
    super();
  }

  getPlanetByUrl(url: string): Observable<Planet> {
    return this.http.get<Planet>(url);
  }


  getPlanetFromLocalStorageByURL(url: string): Planet  {
    let planets: Planet[] = [];
    let planet: Planet;

    if (localStorage.getItem('planets') !== null) {

      planets = JSON.parse(localStorage.getItem('planets'));

      for (let i = 0; i < planets.length; i++) {
        if (planets[i].url.localeCompare(url) === 0) {

          planet = planets[i];
        }
      }
    }
    return planet;
  }

}
