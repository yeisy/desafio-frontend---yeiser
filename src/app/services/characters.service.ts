import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseApi } from '../models/responseApi';
import { Character } from '../models/character';
import { LocalStorageService } from '../models/LocalStorage';

@Injectable({
  providedIn: 'root'
})
export class CharactersService extends LocalStorageService {

  constructor(private http: HttpClient) {
    super();
  }

  getCharacters(): Observable<ResponseApi>{
    const url = 'https://swapi.co/api/people/';
    return this.http.get<ResponseApi>(url);
  }

  getCharactersPerPage(page: number): Observable<ResponseApi>{
    const url = 'https://swapi.co/api/people/?page=' + page;
    return this.http.get<ResponseApi>(url);
  }

  getSingleCharacter(url: string): Observable<Character>{
    return this.http.get<Character>(url);
  }


  getCharacterFromLocalStorageByURL(url: string): Character  {
    let characters: Character[] = [];
    let character: Character;

    if (localStorage.getItem('characters') !== null) {

      characters = JSON.parse(localStorage.getItem('characters'));

      for (let i = 0; i < characters.length; i++) {
        if (characters[i].url.localeCompare(url) === 0) {
          character = characters[i];
        }
      }
    }
    return character;
  }
}
