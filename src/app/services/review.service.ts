import { Injectable } from '@angular/core';
import { Reviews } from '../models/reviews';
import { LocalStorageService } from '../models/LocalStorage';

@Injectable({
  providedIn: 'root'
})
export class ReviewService extends LocalStorageService {

  reviews: Array<Reviews> = [];

  constructor() {
    super();
  }

  updateReviewLikes(index: number, likes: boolean){
    if(localStorage.getItem("reviews") !== null){
      this.reviews = JSON.parse(localStorage.getItem("reviews"));
      if(likes){
        this.reviews[index].likes += 1;
      }else{
        this.reviews[index].dislikes += 1;
      }
      localStorage.setItem("reviews", JSON.stringify(this.reviews));
    }
  }

}
