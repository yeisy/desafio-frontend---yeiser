export interface Reviews {
    name: string;
    email: string;
    comment: string;   
    likes: number;
    dislikes: number; 
}
