import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  saveObjectToLocalStorage(object, key, atEnd = true) {
    let objects: object[] = [];

    if (localStorage.getItem(key) === null) {
      (atEnd) ? objects.push(object) : objects.unshift(object) ;
      localStorage.setItem(key, JSON.stringify(objects));
    } else {
      objects = JSON.parse(localStorage.getItem(key));
      (atEnd) ? objects.push(object) : objects.unshift(object) ;
      localStorage.setItem(key, JSON.stringify(objects));
    }
  }

  replaceObjectInLocalStorage(object, key) {
      localStorage.setItem(key, JSON.stringify(object));
  }

  getObjectsFromLocalStorage(key) {
    let objects: object[];

    if (localStorage.getItem(key) === null) {
      return objects;
    } else {
      objects = JSON.parse(localStorage.getItem(key));
      return objects;
    }
  }

  setValue(value, key){
    localStorage.setItem(key, value);
  }

  getValue(key){
    return localStorage.getItem(key);
  }

}
