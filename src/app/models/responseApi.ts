export interface ResponseApi {
    count: number;
    next: string;
    previous: string;
    results: Array<any>;
}
